# 软件开发

## 目录
- [软件开发](#软件开发)
  - [目录](#目录)
  - [介绍](#介绍)
  - [下辖项目仓库](#下辖项目仓库)
  - [模板文件](#模板文件)


## 介绍
北极熊软件开发总仓库，负责整合、链接到各个软件开发项目的仓库。

## 下辖项目仓库
- [串口助手](https://gitee.com/SMBU-POLARBEAR/Serial_Port_Assistant) 接收C板发回的数据并绘制波形图
- [HSV色域转换预览](https://gitee.com/SMBU-POLARBEAR/HSV_Preview) 预览HSV色域中掩膜覆盖效果

## 模板文件
- [ISSUE模板](./.gitee/ISSUE_TEMPLATE.zh-CN.md)（还在编写中）
- [Pull Request模板](./.gitee/PULL_REQUEST_TEMPLATE.zh-CN.md)（还在编写中）
- [gitignore模板](./.gitignore)（还在编写中）
